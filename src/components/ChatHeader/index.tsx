import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import styles from './styles.module.scss';

export interface IChatHeader {
  name: string,
  participants: number,
  messages: number,
  lastMessage: string
}

const ChatHeader = ({
  name,
  participants,
  messages,
  lastMessage
}: IChatHeader): JSX.Element => {
  const messageDate = moment(lastMessage).format('HH:mm');

  return (
    <div className={styles.chatHeader}>
      <div className={styles.chatInfo}>
        <span>{name}</span>
        <span>
          {participants}
          {' '}
          participants
        </span>
        <span>
          {messages}
          {' '}
          messages
        </span>
      </div>
      <div className={styles.lastMessage}>
        <span>
          last message at
          {messageDate}
        </span>
      </div>
    </div>
  );
};

ChatHeader.propTypes = {
  name: PropTypes.string.isRequired,
  participants: PropTypes.number.isRequired,
  messages: PropTypes.number.isRequired,
  lastMessage: PropTypes.string.isRequired
};

export default ChatHeader;
