import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Message, { IMessage, MessageCallbacks } from '../Message';

import styles from './styles.module.scss';

type MessageListProps = {
  messages: IMessage[]
} & MessageCallbacks;

const MessageList: React.FC<MessageListProps> = ({
  messages = [],
  likeMessage,
  deleteMessage,
  editMessage }): JSX.Element => {
  let previousDate: string | undefined;
  if (messages.length) {
    previousDate = messages[0].createdAt;
  }

  const compareDates = (nextDate: string): boolean => {
    if (!previousDate) {
      previousDate = nextDate;
      return false;
    }
    if (moment(previousDate).diff(nextDate, 'days') < 0) {
      return true;
    }
    return false;
  };
  return (
    <div className={styles.messagesScreen}>
      {messages.map(message => {
        const { createdAt } = message;

        const isNextDay = compareDates(createdAt);
        const prev = previousDate;
        previousDate = createdAt;
        return (
          <div key={message.id} className={styles.messageWrap}>
            {isNextDay
              ? (
                <div key={Math.random().toString()} className={styles.messageDateSeparator}>
                  {moment(prev).format('MMMM Do')}
                </div>
              )
              : null}
            <Message
              key={message.id}
              likeMessage={likeMessage}
              deleteMessage={deleteMessage}
              editMessage={editMessage}
              {...message}
            />
          </div>
        );
      })}
    </div>
  );
};

MessageList.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.any).isRequired,
  likeMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired
};

export default MessageList;
