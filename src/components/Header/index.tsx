import React from 'react';

import styles from './styles.module.scss';

const Header = (): JSX.Element => (
  <header className={styles.headerWrp}>
    <div className={styles.logo}>Chatty</div>
  </header>
);

export default Header;
