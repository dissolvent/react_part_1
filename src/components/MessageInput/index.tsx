import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

type MessageInputProps = {
  sendMessage: (message: string) => void
  message: string
}

const MessageInput = ({ sendMessage, message }: MessageInputProps): JSX.Element => {
  const [toSend, setToSend] = useState<string>(message);
  const textArea = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    setToSend(message);
    if (textArea.current !== null) {
      textArea.current.focus();
    }
  }, [message]);

  const handleSend = (text: string) => {
    sendMessage(text);
    setToSend('');
  };

  return (
    <div className={styles.inputWrap}>
      <textarea
        ref={textArea}
        className={styles.inputArea}
        value={toSend}
        placeholder="Write a message"
        onChange={(ev: React.ChangeEvent<HTMLTextAreaElement>) => {
          setToSend(ev.target.value);
        }}
      />
      <button
        type="button"
        className={styles.sendMessage}
        onClick={() => handleSend(toSend)}
      >
        &#8657; Send
      </button>
    </div>
  );
};

MessageInput.propTypes = {
  sendMessage: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired
};

export default MessageInput;
