import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import styles from './styles.module.scss';

export interface IMessage {
  id: string
  text: string
  user: string
  avatar?: string
  userId: string
  editedAt: string
  createdAt: string
  canEdit: boolean
  likesCount: number
}

export interface MessageCallbacks {
  likeMessage: (id: string) => void
  deleteMessage: (id: string) => void
  editMessage: (id: string) => void
}

type MessageProps = IMessage & MessageCallbacks

const Message: React.FC<MessageProps> = ({
  id,
  text,
  avatar,
  createdAt,
  editedAt,
  user,
  canEdit,
  likesCount,
  likeMessage,
  deleteMessage,
  editMessage
}): JSX.Element => {
  const messageStyle = canEdit ? `${styles.message} ${styles.userMessage}` : styles.message;
  const creationTime = moment(createdAt).format('HH:mm');

  const deleteButton = canEdit
    ? <button type="button" className={styles.button} onClick={() => deleteMessage(id)}>X</button>
    : null;

  const editButton = canEdit
    ? <button type="button" className={styles.button} onClick={() => editMessage(id)}>&#9998;</button>
    : null;

  return (
    <div className={messageStyle}>
      {avatar
        ? <img className={styles.avatar} src={avatar} alt="user avatar" />
        : null}
      <div className={styles.messageBody}>
        <div className={styles.messageHeader}>
          <span>{user}</span>
          <div>
            {editButton}
            {deleteButton}
          </div>
        </div>
        <div className={styles.messageText}>
          {text}
        </div>
        <div className={styles.messageExtra}>
          <div className={styles.likeWrap}>
            <button
              type="button"
              className={styles.likeButton}
              onClick={() => likeMessage(id)}
            >
              &#128077;
            </button>
            <div className={styles.likeCount}>{likesCount}</div>
          </div>
          <div className={styles.messageDate}>
            {editedAt.length ? 'edited • ' : null}
            {creationTime}
          </div>
        </div>
      </div>
    </div>
  );
};

Message.defaultProps = {
  avatar: ''
};

Message.propTypes = {
  id: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  createdAt: PropTypes.string.isRequired,
  editedAt: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  canEdit: PropTypes.bool.isRequired,
  likesCount: PropTypes.number.isRequired,
  likeMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired
};

export default Message;
