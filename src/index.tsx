import React from 'react';
import ReactDOM from 'react-dom';
import Chat from './containers/Chat';
import Header from './components/Header';

import './styles/reset.scss';
import './styles/common.scss';

ReactDOM.render(
  <React.StrictMode>
    <Header />
    <Chat />
  </React.StrictMode>,
  document.getElementById('root') as HTMLElement
);
