import React, { useState, useEffect } from 'react';
import ChatHeader from '../../components/ChatHeader';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import { IMessage } from '../../components/Message';
import Spinner from '../../components/Spinner';
import styles from './styles.module.scss';

interface IFetchedMessages {
  id: string
  text: string
  user: string
  avatar: string
  userId: string
  editedAt: string
  createdAt: string
}

const Chat = (): JSX.Element => {
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [inEditMessage, setInEditMessage] = useState<IMessage | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);

  const updateMock = (fetchedMessages: IFetchedMessages[]): IMessage[] => (
    fetchedMessages.map(message => ({
      ...message,
      likesCount: 0,
      canEdit: false
    })));

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json');
        const fetchedMessages = await response.json();
        const updatedMock = updateMock(fetchedMessages);
        setMessages(updatedMock);
        setLoading(false);
      } catch (err) {
        console.warn(err);
      }
    };
    fetchData();
  }, [isLoading]);

  const sendEdited = (toSend: string) => {
    const { id } = inEditMessage as IMessage;
    const updatedMessages = messages.map(message => {
      if (message.id === id) {
        return { ...message, text: toSend, editedAt: new Date().toJSON() };
      }
      return message;
    });
    setMessages(updatedMessages);
    setInEditMessage(null);
  };

  const sendMessage = (message: string) => {
    if (inEditMessage) {
      sendEdited(message);
      return;
    }
    if (message.trim().length) {
      const newMessage = {
        id: Math.random().toString(),
        text: message,
        user: 'Doc',
        userId: 'bla-bla-bla',
        createdAt: new Date().toJSON(),
        editedAt: '',
        canEdit: true,
        likesCount: 0
      };
      setMessages([...messages, newMessage]);
    }
  };

  const likeMessage = (id: string) => {
    const updatedMessages = messages.map(message => {
      if (message.id === id) {
        const likesCount = message.likesCount === 1 ? 0 : 1;
        return { ...message, likesCount };
      }
      return message;
    });
    setMessages(updatedMessages);
  };

  const deleteMessage = (id: string) => {
    const updatedMessages = messages.filter(message => message.id !== id);
    setMessages(updatedMessages);
  };

  const startEditMessage = (id: string) => {
    // eslint-disable-next-line
    const message = messages.find(message => message.id === id);
    if (message) {
      setInEditMessage(message);
    }
  };

  return (
    isLoading
      ? <Spinner />
      : (
        <div className={styles.chatContainer}>
          <ChatHeader
            name="My awesome chat"
            participants={23}
            messages={messages.length}
            lastMessage={messages[messages.length - 1].createdAt}
          />
          <MessageList
            messages={messages}
            likeMessage={likeMessage}
            deleteMessage={deleteMessage}
            editMessage={startEditMessage}
          />
          <MessageInput
            sendMessage={sendMessage}
            message={inEditMessage ? inEditMessage.text : ''}
          />
        </div>
      )
  );
};

export default Chat;
